package com.aait.albaker.Models

import java.io.Serializable

class PackagesResponse:BaseResponse(),Serializable {
    var data:ArrayList<PackageModel>?=null
}