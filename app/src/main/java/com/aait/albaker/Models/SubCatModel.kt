package com.aait.albaker.Models

import java.io.Serializable

class SubCatModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var selected:Boolean?=false
}