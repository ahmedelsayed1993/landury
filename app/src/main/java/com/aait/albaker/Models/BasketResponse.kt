package com.aait.albaker.Models

import java.io.Serializable

class BasketResponse:BaseResponse(),Serializable {
    var carts:ArrayList<CartModel>?=null
    var additionals:ArrayList<AdditionsModel>?=null
    var total:Int?=null
    var delivery:String?=null
    var tax:String?=null
    var price_urgent:String?=null
    var urgent:Int?=null
}