package com.aait.albaker.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var total:String?=null
    var date:String?=null
    var time:String?=null
    var status:String?=null
    var lat:String?=null
    var lng:String?=null
    var address:String?=null
    var username:String?=null
    var image:String?=null
}