package com.aait.albaker.Models

import java.io.Serializable

class ProvidersModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var address:String?=null
    var distance:Int?=null
    var favorite:Int?=null
    var category_id:Int?=null
    var category:String?=null
}