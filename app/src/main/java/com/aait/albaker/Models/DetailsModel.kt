package com.aait.albaker.Models

import java.io.Serializable

class DetailsModel:Serializable {
    var total:String?=null
    var tax:String?=null
    var delivery:String?=null
    var price_urgent:String?=null
    var total_additional:String?=null
    var final_total:String?=null
    var address:String?=null
}