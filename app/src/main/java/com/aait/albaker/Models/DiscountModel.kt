package com.aait.albaker.Models

import java.io.Serializable

class DiscountModel:Serializable {
    var total:String?=null
    var delivery:String?=null
    var discount:String?=null
    var after_discount:Int?=null
    var final_toatal:Int?=null
}