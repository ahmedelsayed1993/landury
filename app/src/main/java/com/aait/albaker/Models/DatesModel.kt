package com.aait.albaker.Models

import java.io.Serializable

class DatesModel:Serializable {
    var id:Int?=null
    var date:String?=null
    var time:String?=null
}