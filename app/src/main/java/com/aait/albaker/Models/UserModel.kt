package com.aait.albaker.Models

import android.provider.Settings
import java.io.Serializable

class UserModel:Serializable {
    var token:String?=null
    var name:String?=null
    var last_name:String?=null
    var phone:String?=null
    var email:String?=null
    var code:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var avatar:String?=null
    var date:String?=null
    var driver:Int?=null
    var user_type:String?=null
    var active:Int?=null
    var confirm:Int?=null
    var city_id:Int?=null
    var city_name:String?=null
    var lat:String?=null
    var lng:String?=null
    var address:String?=null
    var mac_address_id:String?=null

}