package com.aait.albaker.UI.Activities.Main.Client

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.Models.PackageModel
import com.aait.albaker.Models.PackagesResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.UI.Controllers.PackgeAdapter
import com.aait.albaker.Utils.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PackagesActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_packages

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var viewPager: CardSliderViewPager
    lateinit var indicator: CircleIndicator
    override fun initializeComponents() {
      back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        viewPager = findViewById(R.id.viewPager)
        indicator = findViewById(R.id.indicator)
        title.text = getString(R.string.packages)
        back.setOnClickListener { onBackPressed()
        finish()}
        getData()
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Packages(lang.appLanguage)?.enqueue(object : Callback<PackagesResponse> {
            override fun onFailure(call: Call<PackagesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<PackagesResponse>, response: Response<PackagesResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        initSliderAds(response.body()?.data!!)
                    }
                }
            }

        })
    }


    fun initSliderAds(list: java.util.ArrayList<PackageModel>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
            indicator.visibility = View.GONE
        }
        else{
            viewPager.visibility= View.VISIBLE
            indicator.visibility = View.VISIBLE
            viewPager.adapter= PackgeAdapter(mContext!!,list)
            indicator.setViewPager(viewPager)
        }
    }
}