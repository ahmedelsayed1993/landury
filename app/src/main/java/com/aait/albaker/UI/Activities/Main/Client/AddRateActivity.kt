package com.aait.albaker.UI.Activities.Main.Client

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.Models.TermsResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddRateActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_add_rate
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var rating:RatingBar
    lateinit var rating_del:RatingBar
    lateinit var rate:Button
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rating = findViewById(R.id.rating)
        rating_del = findViewById(R.id.rating_del)
        rate = findViewById(R.id.rate)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
        title.text = getString(R.string.Service_evaluation)
        rate.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddRate("Bearer"+user.userData.token,lang.appLanguage,id,rating.rating.toInt(),rating_del.rating.toInt())
                    ?.enqueue(object : Callback<TermsResponse> {
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    startActivity(Intent(this@AddRateActivity,MainActivity::class.java))
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }

    }
}