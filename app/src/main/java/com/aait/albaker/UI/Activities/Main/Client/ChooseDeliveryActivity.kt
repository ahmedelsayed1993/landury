package com.aait.albaker.UI.Activities.Main.Client

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.*
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.Models.BaseResponse
import com.aait.albaker.Models.DetailsResponse
import com.aait.albaker.Models.TaxResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChooseDeliveryActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_choose_delivery
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var receive:RadioButton
    lateinit var delivery:RadioButton
    lateinit var prods_value:TextView
    lateinit var added_value:TextView
    lateinit var added:TextView
    lateinit var delivery_value:TextView
    lateinit var urgent_lay:LinearLayout
    lateinit var urgent_val:TextView
    lateinit var addition_value:TextView
    lateinit var total:TextView
    lateinit var confirm:Button
    lateinit var location_lay:LinearLayout
    lateinit var location:TextView
    var result = ""
    var mLat = ""
    var mLang = ""
    var way = ""
    var id = 0
    var delivery_price = 0
    var total_price = 0.0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        Log.e("id",id.toString())
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        receive = findViewById(R.id.receive)
        delivery = findViewById(R.id.delivery)
        prods_value = findViewById(R.id.prods_value)
        added_value = findViewById(R.id.added_value)
        added = findViewById(R.id.added)
        delivery_value = findViewById(R.id.delivery_value)
        urgent_lay = findViewById(R.id.urgent_lay)
        urgent_val = findViewById(R.id.urgent_val)
        addition_value = findViewById(R.id.addition_value)
        total = findViewById(R.id.total)
        confirm = findViewById(R.id.confirm)
        location_lay = findViewById(R.id.location_lay)
        location = findViewById(R.id.location)
        title.text = getString(R.string.Receiving_method)
        back.setOnClickListener { onBackPressed()
        finish()}
        delivery.setOnClickListener { location_lay.visibility = View.VISIBLE
            getCoast()
        way = "delivery_order"}
        receive.setOnClickListener { location_lay.visibility = View.GONE
            delivery_value.text = delivery_price.toString()+getString(R.string.rs)
            total.text = total_price.toString()+getString(R.string.rs)

            way = "completed"}
        location_lay.setOnClickListener { startActivityForResult(Intent(this,MyAddressesActivity::class.java),1) }
        getData()
        confirm.setOnClickListener {
            if (way.equals("")){
                sendData(null,null,null)
            }else{
                if (result.equals("")){
                    sendData(null,null,null)
                }else{
                    sendData(result,mLat,mLang)
                }
            }
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.UserOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,null,null,null,null)
            ?.enqueue(object : Callback<DetailsResponse>{
                @SuppressLint("SetTextI18n")
                override fun onResponse(
                    call: Call<DetailsResponse>,
                    response: Response<DetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            prods_value.text = response.body()?.data?.total+getString(R.string.rs)
                            added.text = response.body()?.data?.tax+getString(R.string.rs)
                            delivery_value.text = response.body()?.data?.delivery+getString(R.string.rs)
                            delivery_price = response.body()?.data?.delivery!!.toInt()
                            addition_value.text = response.body()?.data?.total_additional+getString(R.string.rs)
                            total.text = response.body()?.data?.final_total+getString(R.string.rs)
                            total_price = response.body()?.data?.final_total!!.toDouble()
                            urgent_val.text = response.body()?.data?.price_urgent+getString(R.string.rs)
                            if (response.body()?.data?.price_urgent.equals("0")){
                                urgent_lay.visibility = View.GONE
                            }else{
                                urgent_lay.visibility = View.VISIBLE
                            }
                            location.text = response.body()?.data?.address
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<DetailsResponse>, t: Throwable) {
                    t.printStackTrace()
                    CommonUtil.handleException(mContext,t)
                    hideProgressDialog()
                }

            })
    }
    fun sendData(address:String?,lat:String?,lng:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.UserOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,way,lat,lng,address)
            ?.enqueue(object :Callback<DetailsResponse>{
                override fun onResponse(
                    call: Call<DetailsResponse>,
                    response: Response<DetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            startActivity(Intent(this@ChooseDeliveryActivity,MainActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<DetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

            })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result")!!
                mLat = data?.getStringExtra("lat")!!
                mLang = data?.getStringExtra("lng")!!
                location.text = result
            } else {
                result = result
                mLat = mLat
                mLang = mLang
                location.text = result
            }
        }
    }
    fun getCoast(){
        Client.getClient()?.create(Service::class.java)?.Coasts("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object :Callback<TaxResponse>{
            override fun onResponse(call: Call<TaxResponse>, response: Response<TaxResponse>) {
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        delivery_value.text = (delivery_price+(response.body()?.data?.delivery_price!!.toInt())).toString()+getString(R.string.rs)
                        total.text = (total_price+(response.body()?.data?.delivery_price!!.toInt())).toString()+getString(R.string.rs)

                    }else{

                    }
                }
            }

            override fun onFailure(call: Call<TaxResponse>, t: Throwable) {
                t.printStackTrace()
                CommonUtil.handleException(mContext,t)
            }

        })
    }
}