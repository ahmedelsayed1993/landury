package com.aait.albaker.UI.Controllers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.albaker.R
import com.aait.albaker.UI.Fragments.CurrentFragment
import com.aait.albaker.UI.Fragments.FinishedFragment
import com.aait.albaker.UI.Fragments.PendingFragment


class OrderTapAdapter (
        private val context: Context,
        fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            PendingFragment()
        } else if (position == 1){
            CurrentFragment()
        }
        else {
            FinishedFragment()
        }

    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.pending_payment)
        }else if (position == 1){
            context.getString(R.string.current_orders)
        }
        else {
            context.getString(R.string.finished)
        }

    }
}
