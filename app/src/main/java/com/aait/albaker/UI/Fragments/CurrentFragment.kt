package com.aait.albaker.UI.Fragments

import android.content.Intent
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.albaker.Base.BaseFragment
import com.aait.albaker.Listeners.OnItemClickListener
import com.aait.albaker.Models.OrderModel
import com.aait.albaker.Models.OrdersResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.UI.Activities.Main.Client.OrderDetailsActivity
import com.aait.albaker.UI.Controllers.OrderAdapter
import com.aait.albaker.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CurrentFragment : BaseFragment() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.app_recycle

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var orderAdapter: OrderAdapter
    var orderModels = ArrayList<OrderModel>()
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        orderAdapter = OrderAdapter(mContext!!,orderModels, R.layout.recycle_orders)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()
    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Orders(lang.appLanguage,"Bearer"+user.userData.token,"current")?.enqueue(object:
                Callback<OrdersResponse> {
            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<OrdersResponse>,
                    response: Response<OrdersResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            orderAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, OrderDetailsActivity::class.java)
        intent.putExtra("id",orderModels.get(position).id)
        startActivity(intent)
    }
}