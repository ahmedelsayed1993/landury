package com.aait.albaker.UI.Fragments.Delegate

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.aait.albaker.Base.BaseFragment
import com.aait.albaker.R
import com.aait.albaker.UI.Controllers.OrderTapAdapter
import com.aait.albaker.UI.Controllers.TapAdapter
import com.aait.albaker.UI.Fragments.Delegate.OrdersFragment
import com.google.android.material.tabs.TabLayout

class OrdersFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_orders
    companion object {
        fun newInstance(): OrdersFragment {
            val args = Bundle()
            val fragment = OrdersFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: TapAdapter? = null
    override fun initializeComponents(view: View) {
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter = TapAdapter(mContext!!,childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
    }
}