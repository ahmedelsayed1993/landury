package com.aait.albaker.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.albaker.Base.ParentRecyclerAdapter
import com.aait.albaker.Base.ParentRecyclerViewHolder
import com.aait.albaker.Models.OrderModel
import com.aait.albaker.R
import com.bumptech.glide.Glide

class DelegateOrdersAdapter (context: Context, data: MutableList<OrderModel>, layoutId: Int) :
        ParentRecyclerAdapter<OrderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.num!!.setText(mcontext.getString(R.string.order_number)+questionModel.id.toString())
        viewHolder.price.text = questionModel.total+mcontext.getString(R.string.rs)
        Glide.with(mcontext).asBitmap().load(questionModel.image).into(viewHolder.image)
        if (questionModel.status.equals("accepted")||questionModel.status.equals("current")||questionModel.status.equals("delegate_accepted")||
            questionModel.status.equals("received_order_client") || questionModel.status.equals("delivery_order_provider")||
            questionModel.status.equals("finished")   ){
            viewHolder.status.text = "("+mcontext.getString(R.string.delivery_request)+")"
            viewHolder.one.text = mcontext.getString(R.string.delivery_location)
            viewHolder.two.text = mcontext.getString(R.string.Laundry_location)
        }else{
            viewHolder.status.text = "("+mcontext.getString(R.string.Receipt_request)+")"
            viewHolder.two.text = mcontext.getString(R.string.delivery_location)
            viewHolder.one.text = mcontext.getString(R.string.Laundry_location)
        }
        viewHolder.name.text = questionModel.username
        viewHolder.itemView.setOnClickListener(View.OnClickListener {
            view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var num=itemView.findViewById<TextView>(R.id.num)
        internal var price=itemView.findViewById<TextView>(R.id.price)
        internal var status = itemView.findViewById<TextView>(R.id.status)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var one = itemView.findViewById<TextView>(R.id.one)
        internal var two = itemView.findViewById<TextView>(R.id.two)


    }
}