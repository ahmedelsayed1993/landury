package com.aait.albaker.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.albaker.Base.ParentRecyclerAdapter
import com.aait.albaker.Base.ParentRecyclerViewHolder
import com.aait.albaker.Models.ProductModel
import com.aait.albaker.Models.ProvidersModel
import com.aait.albaker.R
import com.bumptech.glide.Glide

class ProvidersAdapter (context: Context, data: MutableList<ProvidersModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProvidersModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.avatar).into(viewHolder.image)
        viewHolder.address.text = questionModel.address
        viewHolder.distance.text = questionModel.distance.toString()+mcontext.getString(R.string.km)

        if (questionModel.favorite==0){
            viewHolder.fav.setImageResource(R.mipmap.fav)
        }else{
            viewHolder.fav.setImageResource(R.mipmap.favactive)
        }

        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.fav.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var address = itemView.findViewById<TextView>(R.id.address)
        internal var distance = itemView.findViewById<TextView>(R.id.distance)
        internal var fav = itemView.findViewById<ImageView>(R.id.fav)





    }
}