package com.aait.albaker.UI.Activities.Main.Delegate

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.Models.CartModel
import com.aait.albaker.Models.DelegateOrderDetailsResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.UI.Controllers.OrderProductAdapter
import com.aait.albaker.Utils.CommonUtil
import com.aait.albaker.Utils.PermissionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewOrderDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_delegate_details
    var id = 0
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var name:TextView
    lateinit var phone:TextView
    lateinit var receive_date:TextView
    lateinit var delivery_date:TextView
    lateinit var orders:RecyclerView
    lateinit var location:TextView
    lateinit var payment:TextView
    lateinit var notes:TextView
    lateinit var prods_value:TextView
    lateinit var added_value:TextView
    lateinit var added:TextView
    lateinit var receive_address:TextView
    lateinit var delivery_location:TextView
    lateinit var delivery_value:TextView
    lateinit var total:TextView
    lateinit var accept:Button
    lateinit var refuse:Button
    lateinit var provider:TextView
    lateinit var orderProductAdapter: OrderProductAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var urgent_lay:LinearLayout
    lateinit var urgent_val:TextView
    lateinit var text:TextView
    lateinit var text1:TextView
    var cartModels = ArrayList<CartModel>()
    var lat = ""
    var lng = ""
    var pro_lat = ""
    var status = ""
    var pro_lng = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        receive_date = findViewById(R.id.receive_date)
        delivery_date = findViewById(R.id.delivery_date)
        orders = findViewById(R.id.orders)
        location = findViewById(R.id.location)
        payment = findViewById(R.id.payment)
        notes = findViewById(R.id.notes)
        prods_value = findViewById(R.id.prods_value)
        added_value = findViewById(R.id.added_value)
        added = findViewById(R.id.added)
        delivery_value = findViewById(R.id.delivery_value)
        total = findViewById(R.id.total)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        provider = findViewById(R.id.provider)
        urgent_lay = findViewById(R.id.urgent_lay)
        urgent_val = findViewById(R.id.urgent_val)
        receive_address = findViewById(R.id.receive_location)
        delivery_location = findViewById(R.id.delivery_location)
        text = findViewById(R.id.text)
        text1 = findViewById(R.id.text1)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.order_details)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        orderProductAdapter = OrderProductAdapter(mContext,cartModels,R.layout.recycle_cart_products)
        orders.layoutManager = linearLayoutManager
        orders.adapter = orderProductAdapter
        getData()
        phone.setOnClickListener { getLocationWithPermission(phone.text.toString()) }
        location.setOnClickListener {
            startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + user.userData.lat + "," + user.userData.lng + "&daddr=" + pro_lat + "," + pro_lng)
                    )
            )

        }
        receive_address.setOnClickListener {
            startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + user.userData.lat + "," + user.userData.lng + "&daddr=" + lat + "," + lng)
                    )
            )

        }
        delivery_location.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=" + user.userData.lat + "," + user.userData.lng + "&daddr=" + pro_lat + "," + pro_lng)
                )
            )
        }
        accept.setOnClickListener {
            if (status.equals("current")||status.equals("accepted")) {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(
                    lang.appLanguage,
                    "Bearer" + user.userData.token,
                    id,
                    "delegate_accepted"
                )
                    ?.enqueue(object : Callback<DelegateOrderDetailsResponse> {
                        override fun onFailure(
                            call: Call<DelegateOrderDetailsResponse>,
                            t: Throwable
                        ) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext, t)
                            t.printStackTrace()
                        }

                        override fun onResponse(
                            call: Call<DelegateOrderDetailsResponse>,
                            response: Response<DelegateOrderDetailsResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful) {
                                if (response.body()?.value.equals("1")) {
                                    //  CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    val intent = Intent(
                                        this@NewOrderDetailsActivity,
                                        OrderAcceptedActivity::class.java
                                    )
                                    intent.putExtra("id", id)
                                    startActivity(intent)
                                    finish()
                                } else {
                                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                }
                            }
                        }

                    })
            }else if (status.equals("delivery_order")){
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(
                    lang.appLanguage,
                    "Bearer" + user.userData.token,
                    id,
                    "accepted_delivery"
                )
                    ?.enqueue(object : Callback<DelegateOrderDetailsResponse> {
                        override fun onFailure(
                            call: Call<DelegateOrderDetailsResponse>,
                            t: Throwable
                        ) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext, t)
                            t.printStackTrace()
                        }

                        override fun onResponse(
                            call: Call<DelegateOrderDetailsResponse>,
                            response: Response<DelegateOrderDetailsResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful) {
                                if (response.body()?.value.equals("1")) {
                                    //  CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    val intent = Intent(
                                        this@NewOrderDetailsActivity,
                                        OrderAcceptedActivity::class.java
                                    )
                                    intent.putExtra("id", id)
                                    startActivity(intent)
                                    finish()
                                } else {
                                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                }
                            }
                        }

                    })
            }
        }
        refuse.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,"delegate_refused")
                    ?.enqueue(object : Callback<DelegateOrderDetailsResponse>{
                        override fun onFailure(call: Call<DelegateOrderDetailsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<DelegateOrderDetailsResponse>, response: Response<DelegateOrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                   // CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    startActivity(Intent(this@NewOrderDetailsActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,null)
                ?.enqueue(object : Callback<DelegateOrderDetailsResponse>{
                    override fun onFailure(call: Call<DelegateOrderDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<DelegateOrderDetailsResponse>, response: Response<DelegateOrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                status = response.body()?.data?.status!!
                                name.text = response.body()?.data?.username
                                phone.text = response.body()?.data?.phone
                                provider.text = response.body()?.data?.provide_name
                                delivery_date.text = response.body()?.data?.delivery_date
                                receive_date.text = response.body()?.data?.received_date
                                receive_address.text = response.body()?.data?.address
                                if (response.body()?.data?.status!!.equals("accepted")||response.body()?.data?.status!!.equals("current")||
                                    response.body()?.data?.status!!.equals("delegate_accepted")||response.body()?.data?.status!!.equals("received_order_client")||
                                    response.body()?.data?.status!!.equals("delivery_order_provider")||response.body()?.data?.status!!.equals("finished")) {
                                    receive_address.text = response.body()?.data?.address
                                    delivery_location.text = response.body()?.data?.provide_address
                                    text.text = getString(R.string.client_location)
                                    text1.text = getString(R.string.Laundry_location)
                                    lat = response.body()?.data?.lat!!
                                    lng = response.body()?.data?.lng!!
                                    pro_lat = response.body()?.data?.provide_lat!!
                                    pro_lng = response.body()?.data?.provide_lng!!
                                }else{
                                    receive_address.text = response.body()?.data?.provide_address
                                    delivery_location.text = response.body()?.data?.address
                                    text.text = getString(R.string.Laundry_location)
                                    text1.text = getString(R.string.client_location)
                                    lat = response.body()?.data?.provide_lat!!
                                    lng = response.body()?.data?.provide_lng!!
                                    pro_lat = response.body()?.data?.lat!!
                                    pro_lng = response.body()?.data?.lng!!
                                }
                                notes.text = response.body()?.data?.notes
                                urgent_val.text = response.body()?.data?.urgent_price+getString(R.string.rs)
                                location.text = response.body()?.data?.provide_address
                                if (response.body()?.data?.urgent_price.equals("0")){
                                    urgent_lay.visibility = View.GONE
                                }else{
                                    urgent_lay.visibility = View.VISIBLE
                                }
                                payment.text = response.body()?.data?.payment
                                prods_value.text = response.body()?.data?.total+getString(R.string.rs)
                                added_value.text = getString(R.string.Value_Added)+"("+response.body()?.data?.tax+"%)"
                                added.text = response.body()?.data?.total_tax+getString(R.string.rs)
                                delivery_value.text = response.body()?.data?.total_delivery+getString(R.string.rs)
                                total.text = response.body()?.data?.final_total+getString(R.string.rs)
                                orderProductAdapter.updateAll(response.body()?.data?.products!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }
}