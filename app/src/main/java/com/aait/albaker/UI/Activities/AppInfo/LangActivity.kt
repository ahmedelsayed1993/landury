package com.aait.albaker.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.R
import com.aait.albaker.UI.Activities.SplashActivity

class LangActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_lang
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var lang1:TextView
    lateinit var lay:RadioGroup
    lateinit var arabic:RadioButton
    lateinit var english:RadioButton


    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        lang1 = findViewById(R.id.lang)
        lay = findViewById(R.id.lay)
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.language)
        if (lang.appLanguage == "ar"){
            lang1.text = getString(R.string.arabic)
            arabic.isChecked = true
        }else{
            lang1.text = getString(R.string.english)
            english.isChecked = true
        }
        arabic.setOnClickListener {
            lang.appLanguage = "ar"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
        english.setOnClickListener {
            lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
        lang1.setOnClickListener { lay.visibility = View.VISIBLE }
    }
}