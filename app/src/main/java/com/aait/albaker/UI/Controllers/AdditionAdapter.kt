package com.aait.albaker.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.albaker.Base.ParentRecyclerAdapter
import com.aait.albaker.Base.ParentRecyclerViewHolder
import com.aait.albaker.Models.AdditionsModel
import com.aait.albaker.Models.HomeModel
import com.aait.albaker.R
import com.bumptech.glide.Glide

class AdditionAdapter (context: Context, data: MutableList<AdditionsModel>, layoutId: Int) :
        ParentRecyclerAdapter<AdditionsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        viewHolder.addition.text = questionModel.text
        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)

        viewHolder.addition.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var addition = itemView.findViewById<TextView>(R.id.addition)




    }
}