package com.aait.albaker.UI.Activities.Main.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.GPS.GPSTracker
import com.aait.albaker.GPS.GpsTrakerListener
import com.aait.albaker.Listeners.OnItemClickListener
import com.aait.albaker.Models.*
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.UI.Controllers.ProvidersAdapter
import com.aait.albaker.Utils.CommonUtil
import com.aait.albaker.Utils.DialogUtil
import com.aait.albaker.Utils.PermissionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class ProvidersActivity:ParentActivity(), OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_providers
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var subs: RecyclerView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var request: Button
    private var mAlertDialog: AlertDialog? = null
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    lateinit var providersAdapter:ProvidersAdapter
    var providersModels = ArrayList<ProvidersModel>()
    lateinit var cat:HomeModel
    override fun initializeComponents() {
        cat  = intent.getSerializableExtra("cat") as HomeModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rv_recycle = findViewById(R.id.rv_recycle)
        tvNoContent = findViewById(R.id.tv_no_content)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = cat.name
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_providers)
        providersAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = providersAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission(null)

        }

        getLocationWithPermission(null)

    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.fav) {

            if (user.loginStatus!!) {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Favourite(
                    lang.appLanguage,
                    "Bearer" + user.userData.token,
                    providersModels.get(position).id!!
                ,cat.id!!)?.enqueue(object : Callback<FavResponse> {
                    override fun onFailure(call: Call<FavResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<FavResponse>,
                        response: Response<FavResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful) {
                            if (response.body()?.value.equals("1")) {
                                getLocationWithPermission(null)
                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            } else {
                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            }
                        }
                    }

                })
            } else {
                CommonUtil.makeToast(mContext!!, getString(R.string.sorry_visitor))
            }
        }

        else {
            val intent = Intent(this, SubCategoryActivity::class.java)
            intent.putExtra("cat", cat.id)
            intent.putExtra("provider", providersModels.get(position).id)
            intent.putExtra("favo", 0)
            startActivity(intent)
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission(cat:Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat)
        }

    }

    internal fun getCurrentLocation(cat:Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (user.loginStatus!!) {
                    getData("Bearer"+user.userData.token,gps.getLatitude().toString(), gps.getLongitude().toString(),cat)
                }else{
                    getData(null,gps.getLatitude().toString(), gps.getLongitude().toString(),cat)
                }
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(token:String?,lat:String,lng:String,id:Int?){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Providers(lang.appLanguage,token,cat.id!!,lat,lng)?.enqueue(object:
            Callback<ProvidersResponse> {
            override fun onFailure(call: Call<ProvidersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ProvidersResponse>,
                response: Response<ProvidersResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){

                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            providersAdapter.updateAll(response.body()!!.data!!)
                        }


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}