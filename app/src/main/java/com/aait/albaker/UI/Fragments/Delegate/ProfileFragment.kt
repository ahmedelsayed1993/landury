package com.aait.albaker.UI.Fragments.Delegate

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.albaker.Base.BaseFragment
import com.aait.albaker.Models.TermsResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.UI.Activities.AppInfo.*
import com.aait.albaker.UI.Activities.Auth.ProfileActivity
import com.aait.albaker.UI.Activities.SplashActivity
import com.aait.albaker.UI.Fragments.Delegate.ProfileFragment
import com.aait.albaker.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_profile_delegate
    companion object {
        fun newInstance(): ProfileFragment {
            val args = Bundle()
            val fragment = ProfileFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var profile: LinearLayout
    lateinit var complaint:LinearLayout
    lateinit var language: LinearLayout
    lateinit var call_us: LinearLayout
    lateinit var policy: LinearLayout
    lateinit var about_app: LinearLayout
    lateinit var logout: LinearLayout
    override fun initializeComponents(view: View) {
        profile = view.findViewById(R.id.profile)
        language = view.findViewById(R.id.language)
        call_us = view.findViewById(R.id.call_us)
        policy = view.findViewById(R.id.policy)
        about_app = view.findViewById(R.id.about_app)
        complaint = view.findViewById(R.id.complaint)
        logout = view.findViewById(R.id.logout)
        logout.setOnClickListener {
            logout()
        }
        about_app.setOnClickListener { startActivity(Intent(activity,AboutAppActivity::class.java)) }
        policy.setOnClickListener { startActivity(Intent(activity,TermsActivity::class.java)) }
        call_us.setOnClickListener { startActivity(Intent(activity,CallUsActivity::class.java)) }
        complaint.setOnClickListener { startActivity(Intent(activity,ComplainActivity::class.java)) }
        language.setOnClickListener { startActivity(Intent(activity,LangActivity::class.java)) }
        profile.setOnClickListener { startActivity(Intent(activity,ProfileActivity::class.java)) }

    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut("Bearer "+user.userData.token,user.userData.device_id!!,"android",user.userData.mac_address_id!!,lang.appLanguage)?.enqueue(object :
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()
                        CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity!!.finish()
                    }else{
                        user.loginStatus=false
                        user.Logout()
                       // CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity!!.finish()

                    }
                }
            }
        })
    }
}