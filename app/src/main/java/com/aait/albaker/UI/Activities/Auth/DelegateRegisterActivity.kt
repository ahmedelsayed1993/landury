package com.aait.albaker.UI.Activities.Auth

import android.content.Intent
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.*
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.Models.UserResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.UI.Activities.AppInfo.TermsActivity
import com.aait.albaker.UI.Activities.LocationActivity
import com.aait.albaker.Utils.CommonUtil
import com.aait.albaker.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class DelegateRegisterActivity :ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register_delegate
    var lat = ""
    var lng = ""
    var result = ""
    lateinit var back:ImageView
    lateinit var register: Button
    lateinit var phone: EditText
    lateinit var name: EditText
    lateinit var last_name:EditText
    lateinit var ID_num:EditText
    lateinit var iban:EditText
    lateinit var bank_name:EditText
    lateinit var email: EditText
    lateinit var locatrion: TextView
    lateinit var password: EditText
    lateinit var view: ImageView
    lateinit var confirm_password: EditText
    lateinit var ID:ImageView
    lateinit var license:ImageView
    lateinit var car_front:ImageView
    lateinit var car_back:ImageView
    lateinit var car_form:ImageView
    lateinit var selifi:ImageView
    lateinit var check:CheckBox
    lateinit var terms:TextView

    lateinit var login: LinearLayout

    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
            .setRequestCode(200)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath1: String? = null
    internal var returnValue2: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options2 = Options.init()
            .setRequestCode(300)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue2)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath2: String? = null
    internal var returnValue3: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options3 = Options.init()
            .setRequestCode(400)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue3)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath3: String? = null
    internal var returnValue4: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options4 = Options.init()
            .setRequestCode(500)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue4)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath4: String? = null
    internal var returnValue5: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options5 = Options.init()
            .setRequestCode(600)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue5)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath5: String? = null
    var deviceID = ""
    var Id = ""
    override fun initializeComponents() {
        Id =  Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        last_name = findViewById(R.id.last_name)
        ID_num = findViewById(R.id.ID_num)
        iban = findViewById(R.id.iban)
        bank_name = findViewById(R.id.bank_name)
        car_back = findViewById(R.id.car_back)
        car_form = findViewById(R.id.car_form)
        car_front = findViewById(R.id.car_front)
        selifi = findViewById(R.id.selifi)
        email = findViewById(R.id.email)
        locatrion = findViewById(R.id.location)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_password)
        ID = findViewById(R.id.ID)
        license = findViewById(R.id.license)
        login = findViewById(R.id.login)
        back = findViewById(R.id.back)
        check = findViewById(R.id.check)
        terms = findViewById(R.id.terms)
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        back.setOnClickListener { onBackPressed()
            finish()}
        login.setOnClickListener {val intent = Intent(this,LoginActivity::class.java)
            intent.putExtra("type","provider")
            startActivity(intent)
            finish()}


        locatrion.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        ID.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        license.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }
        car_front.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options2)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options2)
            }
        }
        car_back.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options3)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options3)
            }
        }
        car_form.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options4)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options4)
            }
        }
        selifi.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options5)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options5)
            }
        }
        register.setOnClickListener {  if(CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                CommonUtil.checkEditError(last_name,getString(R.string.enter_name))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError(locatrion,getString(R.string.enter_location))||
                CommonUtil.checkEditError(ID_num,getString(R.string.Identification_Number))||
                CommonUtil.checkEditError(iban,getString(R.string.IBAN_number))||
                CommonUtil.checkEditError(bank_name,getString(R.string.bank_name))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
            return@setOnClickListener
        } else{
            if (!password.text.toString().equals(confirm_password.text.toString())) {
                CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
            } else {
                if (ImageBasePath==null){
                    CommonUtil.makeToast(mContext,getString(R.string.ID_photo))
                }else{
                    if (ImageBasePath1==null){
                        CommonUtil.makeToast(mContext,getString(R.string.A_copy_of_the_driver_license))
                    }else{
                        if (ImageBasePath2==null){
                            CommonUtil.makeToast(mContext,getString(R.string.Front_picture_of_the_car))
                        }else{
                            if (ImageBasePath3==null){
                                CommonUtil.makeToast(mContext,getString(R.string.Car_picture_from_the_back))
                            }else{
                                if (ImageBasePath4==null){
                                    CommonUtil.makeToast(mContext,getString(R.string.car_registration_form))
                                }else{
                                    if (ImageBasePath5==null){
                                        CommonUtil.makeToast(mContext,getString(R.string.Selfie_without_glasses))
                                    }else{
                                        Register(ImageBasePath!!,ImageBasePath1!!,ImageBasePath2!!,ImageBasePath3!!,ImageBasePath4!!,ImageBasePath5!!)
                                    }
                                }
                            }
                        }

                    }
                }



            }
        }
        }

    }

    fun Register(path:String,path1:String,path2:String,path3:String,path4:String,path5:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("id_image", ImageFile.name, fileBody)
        var filePart1: MultipartBody.Part? = null
        val ImageFile1 = File(path1)
        val fileBody1 = RequestBody.create(MediaType.parse("*/*"), ImageFile1)
        filePart1 = MultipartBody.Part.createFormData("driving_license", ImageFile1.name, fileBody1)
        var filePart2: MultipartBody.Part? = null
        val ImageFile2 = File(path2)
        val fileBody2 = RequestBody.create(MediaType.parse("*/*"), ImageFile2)
        filePart2 = MultipartBody.Part.createFormData("car_picture_front", ImageFile2.name, fileBody2)
        var filePart3: MultipartBody.Part? = null
        val ImageFile3 = File(path3)
        val fileBody3 = RequestBody.create(MediaType.parse("*/*"), ImageFile3)
        filePart3 = MultipartBody.Part.createFormData("car_picture_behind", ImageFile3.name, fileBody3)
        var filePart4: MultipartBody.Part? = null
        val ImageFile4 = File(path4)
        val fileBody4 = RequestBody.create(MediaType.parse("*/*"), ImageFile4)
        filePart4 = MultipartBody.Part.createFormData("car_registration", ImageFile4.name, fileBody4)
        var filePart5: MultipartBody.Part? = null
        val ImageFile5 = File(path5)
        val fileBody5 = RequestBody.create(MediaType.parse("*/*"), ImageFile5)
        filePart5 = MultipartBody.Part.createFormData("glasses_avatar", ImageFile5.name, fileBody5)
        Client.getClient()?.create(Service::class.java)?.SignUpDel(name.text.toString(),last_name.text.toString(),phone.text.toString(),email.text.toString(),locatrion.text.toString()
                ,lat,lng,ID_num.text.toString(),iban.text.toString(),bank_name.text.toString(),password.text.toString(),deviceID,"android",Id,lang.appLanguage,filePart,filePart1
        ,filePart2,filePart3,filePart4,filePart5)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@DelegateRegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                locatrion.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                locatrion.text = ""
            }
        }else  if (requestCode == 100) {
            if (resultCode==0){

            }else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(ID)

                if (ImageBasePath != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }else if (requestCode == 200) {
            if (resultCode==0){

            }else {
                returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath1 = returnValue1!![0]

                Glide.with(mContext).load(ImageBasePath1).into(license)

                if (ImageBasePath1 != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
        else if (requestCode == 300) {
            if (resultCode==0){

            }else {
                returnValue2 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath2 = returnValue2!![0]

                Glide.with(mContext).load(ImageBasePath2).into(car_front)

                if (ImageBasePath2 != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
        else if (requestCode == 400) {
            if (resultCode==0){

            }else {
                returnValue3 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath3 = returnValue3!![0]

                Glide.with(mContext).load(ImageBasePath3).into(car_back)

                if (ImageBasePath3 != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
        else if (requestCode == 500) {
            if (resultCode==0){

            }else {
                returnValue4 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath4 = returnValue4!![0]

                Glide.with(mContext).load(ImageBasePath4).into(car_form)

                if (ImageBasePath4 != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
        else if (requestCode == 600) {
            if (resultCode==0){

            }else {
                returnValue5 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath5 = returnValue5!![0]

                Glide.with(mContext).load(ImageBasePath5).into(selifi)

                if (ImageBasePath5 != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }

}