package com.aait.albaker.UI.Activities.Main.Client

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.Models.CartModel
import com.aait.albaker.Models.DetailsResponse
import com.aait.albaker.Models.OrderDetailsResponse
import com.aait.albaker.Models.TaxResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.UI.Controllers.OrderProductAdapter
import com.aait.albaker.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_details
    var id = 0
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var one:ImageView
    lateinit var two:ImageView
    lateinit var three:ImageView
    lateinit var four:ImageView
    lateinit var line:TextView
    lateinit var line1:TextView
    lateinit var line2:TextView
    lateinit var text:TextView
    lateinit var text1:TextView
    lateinit var text2:TextView
    lateinit var text3:TextView
    lateinit var orders:RecyclerView
    lateinit var delegate:LinearLayout
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var phone:TextView
    lateinit var num:TextView
    lateinit var price:TextView
    lateinit var date:TextView
    lateinit var time:TextView
    lateinit var finish:Button
    lateinit var rate:Button
    lateinit var orderProductAdapter: OrderProductAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var receive: RadioButton
    lateinit var delivery: RadioButton
    lateinit var prods_value:TextView
    lateinit var added_value:TextView
    lateinit var added:TextView
    lateinit var delivery_value:TextView
    lateinit var urgent_lay:LinearLayout
    lateinit var urgent_val:TextView
    lateinit var addition_value:TextView
    lateinit var total:TextView
    lateinit var confirm:Button
    lateinit var location_lay:LinearLayout
    lateinit var location:TextView
    var result = ""
    var mLat = ""
    var mLang = ""
    var way = ""
    var delivery_price = 0
    var total_price = 0.0
    var cartModels = ArrayList<CartModel>()
    lateinit var delivery_lay:LinearLayout
    lateinit var delivery1:LinearLayout
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        line = findViewById(R.id.line)
        line1 = findViewById(R.id.line1)
        line2 = findViewById(R.id.line2)
        text = findViewById(R.id.text)
        text1 = findViewById(R.id.text1)
        text2 = findViewById(R.id.text2)
        text3 = findViewById(R.id.text3)
        orders = findViewById(R.id.orders)
        delegate = findViewById(R.id.delegate)
        delivery1 = findViewById(R.id.delivery1)
        name = findViewById(R.id.name)
        image = findViewById(R.id.image)
        phone = findViewById(R.id.phone)
        num = findViewById(R.id.num)
        price = findViewById(R.id.price)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)
        finish = findViewById(R.id.finish)
        rate = findViewById(R.id.rate)
        receive = findViewById(R.id.receive)
        delivery = findViewById(R.id.delivery)
        prods_value = findViewById(R.id.prods_value)
        added_value = findViewById(R.id.added_value)
        added = findViewById(R.id.added)
        delivery_value = findViewById(R.id.delivery_value)
        urgent_lay = findViewById(R.id.urgent_lay)
        urgent_val = findViewById(R.id.urgent_val)
        addition_value = findViewById(R.id.addition_value)
        total = findViewById(R.id.total)
        confirm = findViewById(R.id.confirm)
        location_lay = findViewById(R.id.location_lay)
        location = findViewById(R.id.location)
        delivery_lay = findViewById(R.id.delivery_lay)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        orderProductAdapter = OrderProductAdapter(mContext,cartModels,R.layout.recycle_cart_products)
        orders.layoutManager = linearLayoutManager
        orders.adapter = orderProductAdapter
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        title.text = getString(R.string.order_details)
        back.setOnClickListener { onBackPressed()
        finish()}
        delivery.setOnClickListener { location_lay.visibility = View.VISIBLE
            getCoast()
            way = "delivery_order"}
        receive.setOnClickListener { location_lay.visibility = View.GONE
            delivery1.visibility = View.GONE
            total.text = total_price.toString()+getString(R.string.rs)

            way = "completed"}
        location_lay.setOnClickListener { startActivityForResult(Intent(this,MyAddressesActivity::class.java),1) }

        getData()
        finish.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.OrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,"completed")
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                        override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                  val intent = Intent(this@OrderDetailsActivity,AddRateActivity::class.java)
                                    intent.putExtra("id",id)
                                    startActivity(intent)
                                    finish
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }


        rate.setOnClickListener {
            val intent = Intent(this@OrderDetailsActivity,AddRateActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
            finish
        }
        confirm.setOnClickListener {
            if (way.equals("completed")){
                sendData(null,null,null)
            }else{
                if (result.equals("delivery_order")){
                    sendData(null,null,null)
                }else{
                    sendData(result,mLat,mLang)
                }
            }
        }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,null)
                ?.enqueue(object : Callback<OrderDetailsResponse> {
                    override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                Glide.with(mContext).load(response.body()?.data?.delegate_avatar).into(image)
                                name.text = response.body()?.data?.delegate_name
                                phone.text = response.body()?.data?.delegate_phone
                                num.text = getString(R.string.order_number)+response.body()?.data?.id.toString()
                                price.text = response.body()?.data?.total+getString(R.string.rs)
                                date.text = getString(R.string.order_date)+response.body()?.data?.date
                                time.text = getString(R.string.order_time)+response.body()?.data?.time
                                orderProductAdapter.updateAll(response.body()?.data?.products!!)
                                if (response.body()?.data?.category.equals("store")){

                                    two.visibility = View.GONE
                                    line.visibility = View.GONE
                                    text1.visibility = View.GONE


                                }else{
                                    two.visibility = View.VISIBLE
                                    line.visibility = View.VISIBLE
                                    text1.visibility = View.VISIBLE
                                }
                                Log.e("status", response.body()?.data?.status!!)
                                if (response.body()?.data?.status.equals("accepted")||response.body()?.data?.status.equals("delegate_refused")){
                                    one.setImageResource(R.mipmap.orderstatuestwo)
                                    two.setImageResource(R.mipmap.cartstatues)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.GONE
                                    finish.visibility = View.GONE
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("delegate_accepted")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatues)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("received_order_client")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesone)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("delivery_order_provider")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("received_order_provider")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatuesone)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("delivery_order")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatuesthree)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("finished")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatuesthree)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delivery_lay.visibility = View.VISIBLE
                                    confirm.visibility = View.VISIBLE
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("delivery_order_client")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatuesthree)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.VISIBLE
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("completed")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatuesthree)
                                    four.setImageResource(R.mipmap.deliverystatuesthree)
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                    if (response.body()?.data?.is_rate==0){
                                        rate.visibility = View.VISIBLE
                                    }else{
                                        rate.visibility = View.GONE
                                    }
                                }else{
                                    one.setImageResource(R.mipmap.orderstatues)
                                    two.setImageResource(R.mipmap.cartstatues)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.GONE
                                    finish.visibility = View.GONE
                                    rate.visibility = View.GONE
                                    delivery_lay.visibility = View.GONE
                                    confirm.visibility = View.GONE
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
        Client.getClient()?.create(Service::class.java)?.UserOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,null,null,null,null)
            ?.enqueue(object : Callback<DetailsResponse>{
                @SuppressLint("SetTextI18n")
                override fun onResponse(
                    call: Call<DetailsResponse>,
                    response: Response<DetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            prods_value.text = response.body()?.data?.total+getString(R.string.rs)
                            added.text = response.body()?.data?.tax+getString(R.string.rs)
                            delivery1.visibility = View.GONE
                            addition_value.text = response.body()?.data?.total_additional+getString(R.string.rs)
                            total.text = response.body()?.data?.final_total+getString(R.string.rs)
                            total_price = response.body()?.data?.final_total!!.toDouble()
                            urgent_val.text = response.body()?.data?.price_urgent+getString(R.string.rs)
//                            if (response.body()?.data?.price_urgent.equals("0")){
//                                urgent_lay.visibility = View.GONE
//                            }else{
//                                urgent_lay.visibility = View.VISIBLE
//                            }
                            location.text = response.body()?.data?.address
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<DetailsResponse>, t: Throwable) {
                    t.printStackTrace()
                    CommonUtil.handleException(mContext,t)
                    hideProgressDialog()
                }

            })
    }

    fun sendData(address:String?,lat:String?,lng:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.UserOrderDetails("Bearer"+user.userData.token,lang.appLanguage,id,way,lat,lng,address)
            ?.enqueue(object :Callback<DetailsResponse>{
                override fun onResponse(
                    call: Call<DetailsResponse>,
                    response: Response<DetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            if (way.equals("delivery_order")){
                                val intent = Intent(this@OrderDetailsActivity,PaymentDeliveryActivity::class.java)
                                intent.putExtra("id",id)
                                startActivity(intent)
                                finish()
                            }else {
                                startActivity(
                                    Intent(
                                        this@OrderDetailsActivity,
                                        MainActivity::class.java
                                    )
                                )
                                finish()
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<DetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

            })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result")!!
                mLat = data?.getStringExtra("lat")!!
                mLang = data?.getStringExtra("lng")!!
                location.text = result
            } else {
                result = result
                mLat = mLat
                mLang = mLang
                location.text = result
            }
        }
    }
    fun getCoast(){
        Client.getClient()?.create(Service::class.java)?.Coasts("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object :Callback<TaxResponse>{
            override fun onResponse(call: Call<TaxResponse>, response: Response<TaxResponse>) {
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        delivery1.visibility = View.VISIBLE
                        delivery_value.text = (response.body()?.data?.delivery_price!!.toInt()).toString()+getString(R.string.rs)
                        total.text = (total_price+(response.body()?.data?.delivery_price!!.toInt())).toString()+getString(R.string.rs)

                    }else{

                    }
                }
            }

            override fun onFailure(call: Call<TaxResponse>, t: Throwable) {
                t.printStackTrace()
                CommonUtil.handleException(mContext,t)
            }

        })
    }
}