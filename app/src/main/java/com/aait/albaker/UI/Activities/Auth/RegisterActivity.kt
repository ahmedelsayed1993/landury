package com.aait.albaker.UI.Activities.Auth

import android.content.Intent
import android.provider.Settings
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.Listeners.OnItemClickListener
import com.aait.albaker.Models.ListModel
import com.aait.albaker.Models.ListResponse
import com.aait.albaker.Models.UserResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.UI.Activities.AppInfo.TermsActivity

import com.aait.albaker.UI.Activities.LocationActivity
import com.aait.albaker.UI.Views.ListDialog
import com.aait.albaker.Utils.CommonUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_register
    var lat = ""
    var lng = ""
    var result = ""
    lateinit var back:ImageView
    lateinit var register: Button
    lateinit var phone: EditText
    lateinit var name: EditText
    lateinit var last_name:EditText
    lateinit var city:TextView
    lateinit var email: EditText
    lateinit var locatrion: TextView
    lateinit var password: EditText
    lateinit var view: ImageView
    lateinit var confirm_password: EditText
    lateinit var view1: ImageView
    lateinit var listDialog: ListDialog
    lateinit var listModel: ListModel
    var cities = ArrayList<ListModel>()
    lateinit var login: LinearLayout
    lateinit var check:CheckBox
    lateinit var terms:TextView
    var deviceID = ""
    var ID = ""
    override fun initializeComponents() {
        ID =  Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        last_name = findViewById(R.id.last_name)
        city = findViewById(R.id.city)
        email = findViewById(R.id.email)
        locatrion = findViewById(R.id.location)
        password = findViewById(R.id.password)
        back = findViewById(R.id.back)
        check = findViewById(R.id.check)
        terms = findViewById(R.id.terms)
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        back.setOnClickListener { onBackPressed()
        finish()}

        confirm_password = findViewById(R.id.confirm_password)

        login = findViewById(R.id.login)
        login.setOnClickListener {val intent = Intent(this,LoginActivity::class.java)
            intent.putExtra("type","user")
            startActivity(intent)
            finish()}
        city.setOnClickListener { getCities() }

        locatrion.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        register.setOnClickListener {  if(CommonUtil.checkEditError(name,getString(R.string.enter_name))||
            CommonUtil.checkEditError(last_name,getString(R.string.enter_name))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
               ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
            CommonUtil.checkTextError(city,getString(R.string.city))||
                CommonUtil.checkTextError(locatrion,getString(R.string.enter_location))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
            return@setOnClickListener
        } else{
            if (!password.text.toString().equals(confirm_password.text.toString())) {
                CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
            } else {
                if (check.isChecked) {
                    Register()
                }else{
                    CommonUtil.makeToast(mContext,getString(R.string.terms))
                }
            }
        }
        }

    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cities(lang.appLanguage)?.enqueue(object :Callback<ListResponse>{
            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        cities = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@RegisterActivity,cities,getString(R.string.city))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

        })
    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(name.text.toString(),last_name.text.toString(),listModel.id!!,phone.text.toString(),email.text.toString(),locatrion.text.toString()
                ,lat,lng,password.text.toString(),deviceID,"android",ID,lang.appLanguage)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                locatrion.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                locatrion.text = ""
            }
        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            listDialog.dismiss()
            listModel = cities[position]
            city.text = listModel.name
        }
    }

}