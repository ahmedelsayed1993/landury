package com.aait.albaker.UI.Activities.AppInfo

import android.widget.ImageView
import android.widget.TextView
import com.aait.albaker.Base.ParentActivity
import com.aait.albaker.Models.TermsResponse
import com.aait.albaker.Network.Client
import com.aait.albaker.Network.Service
import com.aait.albaker.R
import com.aait.albaker.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AboutAppActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var about:TextView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        about = findViewById(R.id.about)
        getData()
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.about_app)
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.About(lang.appLanguage)?.enqueue(object:
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        about.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}