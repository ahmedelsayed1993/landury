package com.aait.albaker.GPS

interface GpsTrakerListener {
    fun onTrackerSuccess(lat: Double?, log: Double?)

    fun onStartTracker()
}
